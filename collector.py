import ast
from datetime import datetime

from config import *
import os
import pandas as pd
from kafka import KafkaConsumer
import ast
try:
    consumer = KafkaConsumer(
        config['KAFKA']['topic'],
        bootstrap_servers=list(ast.literal_eval(config['KAFKA']['servers']))
    )
except Exception as e:
    print(e)
    exit()

df = pd.DataFrame(columns=ast.literal_eval(config['CLICKHOUSE']['columns']))
count = 0

def to_csv(df):
    file_name = os.path.join(config['DIR']['data'], str('dump_'+str(datetime.now().strftime('%Y%m%d%H%M%S'))+'.csv'))
    df.drop_duplicates(inplace=True)
    df.to_csv(file_name, index=None, sep=config['DF']['sep'], encoding=config['DF']['encoding'])
    df.drop(df.index, inplace=True)
    return df

try:
    for msg in consumer:
        df = df.append(ast.literal_eval(msg.value.decode()), ignore_index=True)
        count += 1
        if count >=int(config['KAFKA']['collect_size']):
            df=to_csv(df)
            count = 0
except:
    to_csv(df)
