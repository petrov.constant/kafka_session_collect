from clickhouse_driver import Client
from datetime import datetime,timedelta
import requests
import sys,os

class AnalyticsErrors:
    def __init__(self,clickhouse_server='datadomain.net',clickhouse_password='pass',clickhouse_user='user',
                 clickhouse_database='rawdb',clickhouse_table='stream',hours=3,
                 durationNight=-12,dayEnd=None,config_file='night.txt'):
        self.config_file=os.path.join(os.path.abspath(os.curdir),str(config_file))
        timesOfDay = datetime.now().strftime("%H:%M")

        if((dayEnd != None)):
            dayEnd = str(dayEnd[:5]+":00")

        if (dayEnd == None)or(timesOfDay <= dayEnd):
            #not set Day Light
            if(self.__writeDayStatus(0)==True):
                self.from_time = (datetime.now() - timedelta(hours=int(durationNight))).strftime("%Y.%m.%d %H:%M:%S")
            else:
                self.from_time = (datetime.now() - timedelta(hours=int(hours)+3)).strftime("%Y.%m.%d %H:%M:%S")
                self.to_time = (datetime.now() - timedelta(hours=int(hours))).strftime("%Y.%m.%d %H:%M:%S")

            try:
                client = Client(clickhouse_server, clickhouse_password, user=clickhouse_user,
                                database=clickhouse_database)
                query = "select domain(dl),ec,uniq(cid) from {}.{} where Date > \'{}\' and Date < \'{}\' " \
                        "and ec like \'%Error %\' and ec !=\'Error 200\' and ec !=\'Error 0\' and ec!=\'Error \' " \
                        "group by  domain(dl), ec order by uniq(cid) Desc, domain(dl)".format(clickhouse_database,
                                                                                              clickhouse_table,
                                                                                              self.from_time,
                                                                                              self.to_time)
                self.errors = client.execute(query)
            except Exception as e:
                print('Ошибка:', e)

        else:
            #Night
            self.__writeDayStatus(1)
            sys.exit(0)

    def __writeDayStatus(self,status):
        status=str(status)
        try:
            with open(self.config_file, 'r') as f:
                old_status=f.read()
        except Exception as e:
            old_status = '0'
            with open(self.config_file, 'w+') as f:
                f.write(old_status)


        if (old_status != status):
            with open(self.config_file, 'w') as f:
                f.write(status)
            changed=True
        else:
            changed=False
        return changed







    def sendTelegram(self,token='',chat_id=''):
        text="{}-{}:\n".format(self.from_time,datetime.now().strftime("%H:%M:%S"))

        if self.errors:
            for site,error,count in self.errors:
                site=site.lower().replace("www.","")
                text+="{}: {}: {}\n".format(site,error.replace("Error ",""), count)
            method='sendMessage'
            response = requests.post(
                url='https://api.telegram.org/bot{0}/{1}'.format(token, method),
                data={'chat_id': chat_id, 'text': text,'parse_mode':'Markdown'}
            ).json()
        else:
            print('No data or connection with Clickhouse')

stream=AnalyticsErrors(dayEnd='21:01',durationNight=10)
print('send')
stream.sendTelegram()

