import ga2clickhouse
conn=ga2clickhouse.GA2clickhouse()
#conn.dump_collect()

conn.sqllite_dump_missed(normalize=[
    # ['cid','cd1'], #cid
    ['cid','cd2'], #cid
    ['cd1','cd3'], #yid
    ['cid','cd5'], #sour
    ['cid','cd6'], #medi
    ['cid','cd7'], #camp
    ['cid','cd8'], #cont
    ['cid','cd9'], #temp
    ['cid','cd10'],#device
    ['dl','cd11'], #len
])
conn.push_dump_file()
