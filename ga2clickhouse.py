import csv
import math
import os
import re
import sqlite3
import sys
import time
from datetime import datetime, timedelta
from random import randint

import numpy as np
import pandas as pd
from clickhouse_driver import Client
from progressbar import ProgressBar

from config import *


class GA2clickhouse():

    def __esc(self, q):
        q = re.sub(r'[^a-zA-Z0-9а-яА-Я\-\/\:\.\,\=\s\;\&\_\?\%\*\=\>\<\!`]\'', '', str(q))
        return q.replace('`', '\'')

    def __init__(self, clickhouse_server=config['CLICKHOUSE']['server'],
                 clickhouse_password=config['CLICKHOUSE']['password'],
                 clickhouse_user=config['CLICKHOUSE']['user'],
                 clickhouse_database=config['CLICKHOUSE']['database'],
                 clickhouse_table=config['CLICKHOUSE']['table'],
                 date_column='Date'):
        self.stop_values = []
        self.date_column = str(date_column)
        self.good_keys = []
        # self.csvFile = os.path.join(os.path.abspath(os.getcwd()), 'data','dump.csv')
        self.csvFile = os.path.join(config['DIR']['data'], 'dump.csv')
        self.confFile = os.path.join(config['DIR']['current'], 'pushing.cfg')
        self.sqllFile = os.path.join(config['DIR']['data'], 'temp.sqlite')
        self.clickhouse_server = str(clickhouse_server)
        self.clickhouse_password = str(clickhouse_password)
        self.clickhouse_user = str(clickhouse_user)
        self.clickhouse_database = str(clickhouse_database)
        self.clickhouse_table = str(clickhouse_table)
        self.client = Client(self.clickhouse_server, password=self.clickhouse_password, user=self.clickhouse_user,
                             database=self.clickhouse_database, send_receive_timeout=60)
        self.dump_size = 10000
        self.final_size = 150000

    def get_columns(self):
        try:
            query = self.client.execute(
                "select name from system.columns where database=\'{0}\' AND table=\'{1}\'".format(
                    self.clickhouse_database, self.clickhouse_table))
            query = [i[0] for i in query]
            return query
        except Exception as e:
            print(e)

    def select(self, query):
        n = 0
        while True:
            try:
                query = self.__esc(query)
                query = self.client.execute(query)
                break
            except Exception as e:
                print(n, e)
                n += 1
                time.sleep(10)

        return query

    def add_stopvalues(self, *values):
        for val in values:
            self.stop_values.append(val)

    def add_goodkeys(self, *keys):
        for key in keys:
            self.good_keys.append(key)

    def __readlog(self):
        try:
            with open(self.confFile, 'r') as f:
                response = f.readline()
        except FileNotFoundError as e:
            with open(self.confFile, 'w') as f:
                f.write('0')
            response = '0'
        return response

    def __writelog(self, txt):
        with open(self.confFile, 'w') as f:
            f.write(txt)

    def __miss_set_status(self, status):
        status = str(status)
        with open(self.missFile, 'w') as f:
            f.write(status)

    def __miss_get_status(self):
        try:
            with open(self.missFile, 'r') as f:
                response = f.readline()
        except FileNotFoundError as e:
            with open(self.missFile, 'w') as f:
                f.write('False')
            response = 'False'
        return response

    def __esapekeys(self, args):
        k = []
        v = []
        for key, value in args.items():
            key = self.__esc(str(key))
            value = self.__esc(str(value))
            if (((key) in self.good_keys) and (value not in self.stop_values)):
                k.append(key)
                v.append(value)
        args = dict(zip(k, v))
        try:
            re.match(r'[A-Z]{2}-[0-9]{1,15}-[0-9]{1,3}', args['CounterId']).group(0)
        except Exception as e:
            print('Bad CounterId')
            exit()
        return args

    def insert_to_clickhouse(self, args):
        self.args = self.__esapekeys(args)
        if (self.args != None):
            try:
                columns = [str(n) for n in self.args.keys()]
                values = [n for n in self.args.values()]
                columns.insert(0, 'Date')
                values.insert(0, datetime.now())

                columns = ", ".join(str(x) for x in columns)
                return self.client.execute("INSERT INTO raw.stream (" + columns + ") VALUES ", [values])
            except Exception as e:
                print(e)
        else:
            print('No CounterId')

    def save_dump(self, args):
        self.args = self.__esapekeys(args)
        sep = config['DF']['sep']
        df = pd.DataFrame(self.args, index=[0], columns=self.good_keys)
        while True:
            try:
                if os.path.isfile(self.csvFile):
                    df_old = self.__read_df_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)

                    if ((set(self.good_keys) != set(df_old)) or (self.date_column not in set(df_old))):
                        self.__writelog('True')
                        os.rename(self.csvFile, (self.csvFile + '_' + str(randint(1, 1000000)) + '_old.csv'))
                        self.__write_df_csv(df, self.csvFile, mode='w', index=False, sep=sep)

                        self.__writelog('False')
                        break
                    else:
                        while True:
                            if 'True' in self.__readlog():
                                df.to_csv(self.csvFile, mode='a', index=False, sep=sep, header=False)
                                break
                        break
                else:
                    while True:
                        if 'False' in self.__readlog():
                            self.__write_df_csv(df, self.csvFile, mode='w', index=False, sep=sep)

                            break
                    break

            except PermissionError as e:
                pass
            except FileNotFoundError as e:
                if not os.path.exists(self.csvFile):
                    os.mkdir(os.path.dirname(self.csvFile))

    def paste_missed(self, CounterId=None, index_column='cid', normalized_column='cd1', time_slice=180):
        start_time = time.time()
        index_column = str(index_column)
        normalized_column = str(normalized_column)
        time_slice = int(time_slice)
        time_slice = (datetime.now() - timedelta(minutes=time_slice)).strftime("%Y.%m.%d %H:%M:%S")
        query = "select {},{} from raw.stream where Date > `{}`".format(index_column, normalized_column, time_slice)
        if (CounterId != None):
            query = (query + ' and CounterId=`{}`').format(CounterId)
        df = self.select(query)
        df = pd.DataFrame(df, columns=[index_column, normalized_column])
        df.loc[df[df[normalized_column].isin(['false'])].index, normalized_column] = None
        df[index_column] = df[index_column].apply(lambda x: str(x).format(x) if not pd.isnull(x) else x)
        df[normalized_column] = df[normalized_column].apply(lambda x: str(x).format(x) if not pd.isnull(x) else x)
        index_dict = df[[index_column, normalized_column]].copy()
        del (df)
        index_dict.dropna(inplace=True)
        index_dict = index_dict[~index_dict.isin(['NaN', 'NaT', None, np.nan, '']).any(axis=1)]
        index_dict.drop_duplicates(subset=[index_column], inplace=True)
        index_dict = index_dict.set_index(index_column).T.to_dict('list')
        for key, value in index_dict.items():
            index_dict[key] = value[0]
        try:
            for key, value in index_dict.items():
                query = ('ALTER TABLE {}.{} UPDATE {} = `{}` WHERE {} = `{}`').format(self.clickhouse_database,
                                                                                      self.clickhouse_table,
                                                                                      normalized_column, value,
                                                                                      index_column, key)
                if (CounterId != None):
                    query = (query + ' and CounterId=`{}`').format(CounterId)
                query = self.__esc(query)
                self.client.execute(query)

        except Exception as e:

            print(e.args)
            exit()
        print("Inserterd for %s sec." % round(time.time() - start_time))

    def __delete_file(self, file):
        n = 1
        while True:
            try:
                os.remove(file)
                break
            except PermissionError as e:
                print(n, file, e.args)
                time.sleep(1)
                n += 1
            except FileNotFoundError as e:
                print(n, file, e.args)
                break

    def __read_df_csv(self, file, sep=config['DF']['sep'], nrows=None, low_memory=False, index_col=None, header='infer',
                      dtype=str):
        n = 1
        while True:
            try:
                if nrows != None:
                    df = pd.read_csv(file,
                                     sep=sep,
                                     low_memory=low_memory,
                                     header=header, encoding=config['DF']['encoding'],
                                     dtype=dtype,
                                     error_bad_lines=False,
                                     warn_bad_lines=True)
                if nrows == None:
                    df = pd.read_csv(file,
                                     sep=sep,
                                     low_memory=low_memory,
                                     index_col=index_col,
                                     header=header, encoding=config['DF']['encoding'],
                                     error_bad_lines=False,
                                     warn_bad_lines=True,
                                     dtype=dtype)
                else:
                    df = pd.read_csv(file,
                                     sep=sep,
                                     nrows=nrows,
                                     low_memory=low_memory, encoding=config['DF']['encoding'],
                                     index_col=index_col, header=header,
                                     error_bad_lines=False,
                                     warn_bad_lines=True,
                                     dtype=dtype)
                return df
            except PermissionError as e:
                print(n, file, e.args)
                time.sleep(1)
                n += 1
            except FileNotFoundError as e:
                print(n, file, e.args)
                break

    def __rename_file(self, file, new_name):
        while os.path.isfile(new_name) == False:
            try:
                os.rename(file, new_name)
                return new_name
            except Exception:
                pass

    def __write_df_csv(self, df, file, sep=config['DF']['sep'], index=None, header=True, mode='w'):
        n = 1
        while True:
            try:
                df.to_csv(file, sep=sep, index=index, encoding=config['DF']['encoding'],
                          header=header, mode=mode, line_terminator=config['DF']['line_terminator'])
                return True
            except PermissionError as e:
                print(n, file, e.args)
                time.sleep(1)
                n += 1

    def dump_missed(self, file=None, CounterId=None, index_column='cid', normalized_column='cd1'):

        if ('False' in self.__miss_get_status()):
            self.__miss_set_status(True)
        else:
            print('I`am exit missing.cfg:', self.__miss_get_status())
            exit()
        sep = config['DF']['sep']
        if file == None:
            dir = os.path.dirname(self.csvFile)
            file = os.path.join(dir, "dump_collected.csv")
            file = self.__rename_file(file, os.path.join(dir, "missing.csv"))

        start_time = time.time()
        index_column = str(index_column)
        normalized_column = str(normalized_column)
        df_finally = self.__read_df_csv(file, sep=sep, index_col=None, low_memory=False)

        df_finally.fillna('', inplace=True)
        df_finally[index_column] = df_finally[index_column].astype('str')
        df_finally[normalized_column] = df_finally[normalized_column].astype('str')

        if CounterId != None:
            counters = [CounterId]
        else:
            counters = df_finally['CounterId'].unique()
        i_count = 0
        for counter in counters:
            i_count += 1
            print(('\n{} counter {} from {}').format(str(i_count), counter, len(counters)))
            # df = pd.DataFrame(df_finally, columns=[index_column, normalized_column])
            df = pd.DataFrame(df_finally[df_finally['CounterId'] == counter],
                              columns=[index_column, normalized_column])
            df.loc[df[df[normalized_column].isin(['false'])].index, normalized_column] = None
            df[index_column] = df[index_column].apply(lambda x: str(x) if not pd.isnull(x) else x)
            df[normalized_column] = df[normalized_column].apply(lambda x: str(x) if not pd.isnull(x) else x)
            index_dict = df[[index_column, normalized_column]].copy()
            del (df)
            index_dict.dropna(inplace=True)
            index_bad = index_dict[index_dict.isin(['NaN', 'NaT', None, np.nan, '', 'nan']).any(axis=1)]
            index_dict = index_dict[~index_dict.isin(['NaN', 'NaT', None, np.nan, '', 'nan']).any(axis=1)]
            index_dict.drop_duplicates(subset=[index_column], inplace=True)
            index_dict = index_dict.set_index(index_column).T.to_dict('list')
            index_bad.drop_duplicates(subset=[index_column], inplace=True)
            index_bad = index_bad.set_index(index_column).T.to_dict('list')
            index_dict = {k: v[0] for k, v in index_dict.items() if k in index_bad}
            i2_count = 0
            bar2 = ProgressBar(maxval=len(index_dict))
            for key, value in index_dict.items():
                i2_count += 1
                df_finally.iloc[df_finally[(df_finally[index_column] == key) & (
                        df_finally['CounterId'] == counter)].index, df_finally.columns.get_loc(
                    normalized_column)] = value
                bar2.update(i2_count)
            bar2.finish()

        file = self.__rename_file(file, os.path.join(dir, "dump_collected.csv"))
        print(("Pasted {}:{} for %s sec \n" % round(time.time() - start_time)).format(index_column, normalized_column))
        self.__write_df_csv(df_finally, file, sep=sep, index=None)
        self.__miss_set_status(False)

    def sqllite_dump_missed(self, **normalize):
        sep = config['DF']['sep']
        dir = os.path.dirname(self.csvFile)
        file = os.path.join(dir, "dump_collected.csv")
        start_time = time.time()

        df = self.__read_df_csv(file, sep=sep, index_col=None, low_memory=False, dtype=str)
        df.fillna('', inplace=True)
        Counters = df['CounterId'].unique()
        normalize = normalize['normalize']
        progress_len = len(Counters) * len(normalize)
        connection = sqlite3.connect(':memory:')
        table_name = 'df_table'
        df.to_sql(table_name, connection, if_exists="replace")
        del (df)

        bar = ProgressBar(max_value=progress_len)
        i = 0
        for index_column, normalized_column in normalize:
            for counter in Counters:
                bar.update(i)
                sql = """
                select DISTINCT {0},{1}
                FROM
                (SELECT {0},{1}
                FROM {2}
                WHERE CounterId='{3}' and {1}!=''
                )
                INNER JOIN
                (SELECT {0} as key,{1} as value
                FROM {2}
                WHERE CounterId='{3}' and {1}=''
                )
                on {0}=key""".format(index_column, normalized_column, table_name, counter)
                inn = pd.io.sql.read_sql(sql, connection, coerce_float=False)
                inn.drop_duplicates(subset=[index_column], inplace=True)
                inn = inn.set_index(index_column).T.to_dict('list')
                # print(sql,index_column, normalized_column, table_name, counter,inn)
                collect = []
                for key, value in inn.items():
                    value = str(value[0])
                    collect.append([value, key, value])
                del (inn)
                # if(len(inn)>10):
                #    print(('{} lines ~ for {} min').format (len(inn),len(inn)*0.12/60))
                # print(collect,index_column,normalized_column,counter)
                connection.executemany(("""
                UPDATE {0} SET {1}=?
                WHERE {2}=? and CounterId='{3}' and {1}!=?""".format(table_name,
                                                                     normalized_column,
                                                                     index_column,
                                                                     counter)), collect)
                i += 1
        bar.finish()
        # print(collect, index_column, normalized_column)
        connection.commit()
        df = pd.read_sql_query("SELECT * FROM " + table_name, connection)
        df.drop(columns='index', inplace=True)
        dir = os.path.dirname(self.csvFile)
        file = os.path.join(dir, 'dump_collected.csv')
        self.__write_df_csv(df, file)
        connection.close()
        print("Pasted for %s min \n" % str((round(time.time() - start_time)) / 60))

    def threed_sqllite_dump_missed(self, *normalize):
        sep = config['DF']['sep']
        dir = os.path.dirname(self.csvFile)
        file = os.path.join(dir, "dump_collected.csv")
        start_time = time.time()

        df = self.__read_df_csv(file, sep=sep, index_col=None, low_memory=False, dtype=str)
        df.fillna('', inplace=True)
        Counters = df['CounterId'].unique()
        progress_len = len(Counters) * len(normalize)
        connection = sqlite3.connect(':memory:', check_same_thread=False)
        table_name = 'df_table'
        df.to_sql(table_name, connection, if_exists="append")
        del (df)

        bar = ProgressBar(max_value=progress_len)
        i = 0

        def df_updater(sql):
            '''
            connection.executemany(("""
                                                            UPDATE {0} SET {1}=? 
                                                            WHERE {2}=? and CounterId='{3}'""".format(table_name,
                                                                                                      normalized_column,
                                                                                                      index_column,
                                                                                                      counter)),
                                       collect)
            '''
            print('--0--', str(len(map_sql)))
            connection.executescript(sql)
            print('--1--')

        from multiprocessing.dummy import Pool as ThreadPool

        for to_normalize in normalize:
            for index_column, normalized_column in to_normalize.items():
                pool = ThreadPool(len(Counters))
                map_sql = []
                for counter in Counters:
                    bar.update(i)
                    sql = """
                            select DISTINCT {0},{1}
                            FROM
                            (SELECT {0},{1}
                                FROM {2}
                                WHERE CounterId='{3}' and {1}!=''
                            )
                            INNER JOIN
                            (SELECT {0} as key,{1} as value
                                FROM {2}
                                WHERE CounterId='{3}' and {1}=''
                            )
                            on {0}=key
                            """.format(index_column, normalized_column, table_name, counter)

                    inn = pd.io.sql.read_sql(sql, connection, coerce_float=False)
                    inn.drop_duplicates(subset=[index_column], inplace=True)
                    inn = inn.set_index(index_column).T.to_dict('list')
                    collect = []
                    sql = ''
                    for key, value in inn.items():
                        inn[key] = value[0]
                        collect.append([inn[key], key])
                        sql = sql + """
                            UPDATE {0} SET {1}='{2}' 
                            WHERE {3}='{4}' and {1}!='{2}' and CounterId='{5}';""".format(table_name,
                                                                                          normalized_column,
                                                                                          inn[key],
                                                                                          index_column,
                                                                                          key,
                                                                                          counter)

                    map_sql.append(sql)
                    i += 1
                pool.map(df_updater, map_sql)
                pool.close()
                pool.join()
                connection.commit()
        bar.finish()

        df = pd.read_sql_query("SELECT * FROM " + table_name, connection)
        df.to_csv('ddf.csv', sep=sep, encoding=config['DF']['encoding'])
        connection.close()
        print("Pasted for %s min \n" % (round(time.time() - start_time) / 60))

    def pool_threed_sqllite_dump_missed(self, *normalize):
        sep = config['DF']['sep']
        dir = os.path.dirname(self.csvFile)
        file = os.path.join(dir, "dump_collected.csv")
        start_time = time.time()

        df = self.__read_df_csv(file, sep=sep, index_col=None, low_memory=False, dtype=str)
        df.fillna('', inplace=True)
        Counters = df['CounterId'].unique()
        progress_len = len(Counters) * len(normalize)
        connection = sqlite3.connect(':memory:')
        table_name = 'df_tab'
        df.to_sql(table_name, connection, if_exists="replace")
        del (df)

        bar = ProgressBar(max_value=progress_len)
        i = 0

        for to_normalize in normalize:
            for index_column, normalized_column in to_normalize.items():
                # pool = Pool(processes=len(Counters))
                # map_sql=[]
                sql = ''
                for counter in Counters:
                    bar.update(i)
                    sql = """
                            select DISTINCT {0},{1}
                            FROM
                            (SELECT {0},{1}
                                FROM {2}
                                WHERE CounterId='{3}' and {1}!=''
                            )
                            INNER JOIN
                            (SELECT {0} as key,{1} as value
                                FROM {2}
                                WHERE CounterId='{3}' and {1}=''
                            )
                            on {0}=key
                            """.format(index_column, normalized_column, table_name, counter)

                    inn = pd.io.sql.read_sql(sql, connection, coerce_float=False)
                    inn.drop_duplicates(subset=[index_column], inplace=True)
                    inn = inn.set_index(index_column).T.to_dict('list')
                    collect = []
                    # sql=''
                    for key, value in inn.items():
                        inn[key] = value[0]
                        # collect.append([inn[key],key])
                        sql = sql + """
                        UPDATE {0} SET {1}='{2}' WHERE {3}='{4}' and {1}!='{2}' and CounterId='{5}';""".format(
                            table_name,
                            normalized_column,
                            inn[key],
                            index_column,
                            key,
                            counter)

                    # map_sql.append(sql)
                    i += 1

                with open('sql.txt', 'a+') as f:
                    f.write(sql)

                # pool.map(df_updater, map_sql)
                # pool.close()
                # pool.join()
                # connection.commit()
        bar.finish()

        exit()

        df = pd.read_sql_query("SELECT * FROM " + table_name, connection)
        df.to_csv('ddf.csv', sep=sep, encoding=config['DF']['encoding'])
        connection.close()
        print("Pasted for %s min \n" % (round(time.time() - start_time) / 60))

    def dump_collect(self):
        sep = config['DF']['sep']
        # df = pd.read_csv(self.csvFile, sep=sep, index_col=None, low_memory=False)
        df = self.__read_df_csv(self.csvFile, sep=sep, index_col=None, low_memory=False)
        df.dropna(subset=['Date', 'CounterId'], inplace=True)
        size = math.ceil(len(df) / self.dump_size)
        if (size > 1):
            for i in range(1, size):
                df_cut, df = df[:self.dump_size], df[self.dump_size:]
                name = self.csvFile + '_' + str(i) + '.csv'
                if os.path.exists(name):
                    name = self.csvFile + '_' + str(randint(1, 100000)) + '.csv'
                self.__write_df_csv(df_cut, name, sep=sep, index=None)
                # df_cut.to_csv(name, sep=sep, index=None)
                del (df_cut)
            self.__write_df_csv(df, self.csvFile, sep=sep, index=None)

        dir_file = []
        dir = os.path.dirname(self.csvFile)
        for file in os.listdir(dir):
            file = re.search(r"(.*)dump.csv_(.*)", file)

            if file and '#' not in file.group(0) and 'push' not in file.group(0):
                dir_file.append(file.group(0))
        if len(dir_file) == 0:
            print('No files for dump. I am exit')
            exit()

        collect_file = os.path.join(dir, "dump_collected.csv")
        if os.path.isfile(collect_file):
            # df_finally = pd.read_csv(collect_file,index_col=None,sep=sep, low_memory=False)
            df_finally = self.__read_df_csv(collect_file, index_col=None, sep=sep, low_memory=False)
        else:
            df_finally = pd.DataFrame(columns=self.good_keys)
        for file in dir_file:
            file = os.path.join(dir, file)
            df = self.__read_df_csv(file, sep=sep, index_col=None, low_memory=False)
            # df = pd.read_csv(file, sep=sep, index_col=None, low_memory=False)
            try:
                df[self.date_column] = pd.to_datetime(df[self.date_column], format="%Y.%m.%d %H:%M:%S", errors='coerce')
            except KeyError as e:
                if self.date_column not in df.columns:
                    col = self.__read_df_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
                    # col=pd.read_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
                    df = self.__read_df_csv(file, sep=sep, index_col=None, header=None, low_memory=False)
                    # df = pd.read_csv(file, sep=sep, index_col=None,header=None, low_memory=False)
                    df.columns = col.columns
                    df[self.date_column] = pd.to_datetime(df[self.date_column], format="%Y.%m.%d %H:%M:%S",
                                                          errors='coerce')
            df_finally = pd.concat([df_finally, df])
            del (df)
        df_finally[self.date_column] = pd.to_datetime(df_finally[self.date_column], format="%Y.%m.%d %H:%M:%S",
                                                      errors='coerce')
        df_finally = df_finally[~df_finally[self.date_column].isin(['NaN', 'NaT', None, np.nan, ''])]

        '''
        if len(df_finally) > self.final_size:
            self.__write_df_csv(df_finally[:self.final_size],collect_file,sep=sep, header=True, index=None)
            #df_finally[:self.final_size].to_csv(collect_file,sep=sep, header=True, index=None)
            df_finally=df_finally[self.final_size:]
            self.__write_df_csv(df_finally,os.path.join(dir,dir_file[0]), sep=sep, header=True, index=None)
            #df_finally.to_csv( os.path.join(dir,dir_file[0]), sep=sep, header=True, index=None)
            dir_file = dir_file[1:]
        else:
            self.__write_df_csv(df_finally, collect_file, sep=sep, header=True, index=None)
            #df_finally.to_csv(collect_file, sep=sep, header=True, index=None)
        '''
        self.__write_df_csv(df_finally, collect_file, sep=sep, header=True, index=None)

        for file in dir_file:
            file = os.path.join(dir, file)
            self.__delete_file(file)
            '''
 
            while os.path.isfile(collect_file) == False:
                try:
                    os.rename(file, collect_file)
                    break
                except Exception:
                    pass
            '''

    def paste_missed_day(self, CounterId=None, index_column='cid', normalized_column='cd1', time_slice=1440):
        start_time = time.time()
        index_column = str(index_column)
        normalized_column = str(normalized_column)
        time_slice = int(time_slice)
        time_slice = (datetime.now() - timedelta(minutes=time_slice)).strftime("%Y.%m.%d %H:%M:%S")
        query = "select {},{} from raw.stream where Date > `{}`".format(index_column, normalized_column, time_slice)
        if (CounterId != None):
            query = (query + ' and CounterId=`{}`').format(CounterId)
        df = self.select(query)
        df = pd.DataFrame(df, columns=[index_column, normalized_column])
        df.loc[df[df[normalized_column].isin(['false'])].index, normalized_column] = None
        df[index_column] = df[index_column].apply(lambda x: str(x).format(x) if not pd.isnull(x) else x)
        df[normalized_column] = df[normalized_column].apply(lambda x: str(x).format(x) if not pd.isnull(x) else x)
        index_dict = df[[index_column, normalized_column]].copy()
        del (df)
        index_dict.dropna(inplace=True)
        index_dict = index_dict[~index_dict.isin(['NaN', 'NaT', None, np.nan, '']).any(axis=1)]
        index_dict.drop_duplicates(subset=[index_column], inplace=True)
        index_dict = index_dict.set_index(index_column).T.to_dict('list')

        with open("push_paste_missed.csv", 'w', newline='') as f:
            w = csv.writer(f)
            w.writerow([index_column, normalized_column])
            for key, val in index_dict.items():
                w.writerow([key, val[0]])

        max_keys = len(index_dict)
        if max_keys == 0:
            print("Dict {", index_column, ":", normalized_column, "}")
            self.__delete_file("push_paste_missed.csv")
            exit()

        del (index_dict)
        bar = ProgressBar(max_value=max_keys)
        keys_change = 500
        while True:
            try:
                df = self.__read_df_csv("push_paste_missed.csv", sep=',', low_memory=False)
                # df = pd.read_csv("push_paste_missed.csv", sep=',', low_memory=False)
                len_df = len(df)
                if len_df == 0:
                    self.__delete_file("push_paste_missed.csv")
                    print(("Paste missed {}:{} complete for %s sec." % round(time.time() - start_time)).format(
                        index_column, normalized_column))
                    break
                bar.update(max_keys - len_df)

                key = list(df.iloc[:keys_change][index_column])
                val = list(df.iloc[:keys_change][normalized_column])
                df = df.drop(df.index[:keys_change])

                di = dict(zip(key, val))
                query = ''
                for k, v in di.items():
                    q = ('ALTER TABLE {}.{} UPDATE {} = `{}` WHERE {} = `{}`').format(self.clickhouse_database,
                                                                                      self.clickhouse_table,
                                                                                      normalized_column, v,
                                                                                      index_column, k)
                    if (CounterId != None):
                        q = (q + ' and CounterId=`{}`').format(CounterId)
                    query += q + '; '

                # print(query)
                self.__write_df_csv(df, "push_paste_missed.csv", sep=',', header=True, index=None)
                # df.to_csv("push_paste_missed.csv",sep=',',header=True,index=None)
            except Exception as e:
                print(e.args)
                break
            try:
                query = self.__esc(query)
                # self.client.execute(query)
                command = ("clickhouse-client --multiline --multiquery --query=\"{}\"").format(query)

                if 'l' in sys.argv:
                    while True:
                        try:
                            result = str(os.system(command))
                            if result != '0':
                                print('I am sleep. Result:', result)
                                time.sleep(10)
                        except Exception as e:
                            print('Exception:', result)
                            print(e)
                            break
                        break
                else:
                    try:
                        result = str(os.system(command))
                        if result != '0':
                            print('I am exit. Result:', result)
                            exit()
                    except Exception as e:
                        print('Exception:', result)
                        print(e)
                        exit()
                time.sleep(1)


            except Exception as e:
                # print(re.search(r'(.*)\n', e.args[0]).group(0))
                print(e.args)
                time.sleep(60)

    def push_dump(self):
        dir = os.path.dirname(self.csvFile)
        dir_file = []
        for file in os.listdir(dir):
            file = re.search(r"(.*)dump(.*)", file)
            if (file):
                if '#' not in file.group(0) and 'psuh' not in file.group(0):
                    dir_file.append(file.group(0))
        sep = config['DF']['sep']

        for file in dir_file:
            file = os.path.join(dir, file)
            push_file = os.path.join(dir, file) + "_push.csv"

            while os.path.isfile(push_file) == False:
                try:
                    os.rename(file, push_file)
                    break
                except Exception:
                    pass

            df = self.__read_df_csv(push_file, sep=sep, index_col=None, low_memory=False)
            # df = pd.read_csv(push_file, sep=sep, index_col=None, low_memory=False)
            df.dropna(subset=['Date', 'CounterId'], inplace=True)
            size = math.ceil(len(df) / 40000)
            if (size > 1):
                for i in range(1, size):
                    df_cut, df = df[:40000], df[40000:]
                    self.__write_df_csv(df_cut, os.path.abspath(file) + '_' + str(i) + '.csv', sep=sep, index=None)
                    # df_cut.to_csv(os.path.abspath(file)+'_'+str(i)+'.csv', sep=sep,index=None)
                    del (df_cut)
                self.__write_df_csv(df, push_file, sep=sep, index=None)
                # df.to_csv(push_file, sep=sep,index=None)

            try:
                time.sleep(0.3)
                df[self.date_column] = pd.to_datetime(df[self.date_column], format="%Y.%m.%d %H:%M:%S", errors='coerce')

            except ValueError as e:
                print('Bad `Date` format in', push_file)
                exit()
            except KeyError as e:
                if self.date_column not in df.columns:
                    col = self.__read_df_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
                    # col=pd.read_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
                    df.columns = col.columns
                    df[self.date_column] = pd.to_datetime(df[self.date_column], format="%Y.%m.%d %H:%M:%S",
                                                          errors='coerce')

                print('Please set more timing for read', push_file)
                print(e, ":\n", e.args)
                # next()
            df = df[~df[self.date_column].isin(['NaN', 'NaT', None, np.nan, ''])]
            values = []
            for i in range(len(df)):
                values.append(df.iloc[i].values)
            columns = ",".join(df.columns)
            values = []
            for i in range(len(df)):
                val = [n for n in df.iloc[i].values]
                vv = []
                for item in val:
                    if (issubclass(type(item), pd.datetime) == False):
                        item = str(item)
                        item = item.replace('nan', '')
                        item = item.replace('\'', '"')
                    vv.append(item)
                values.append(vv)
            try:
                self.client.execute("INSERT INTO raw.stream (" + columns + ") VALUES ", values)
                time.sleep(0.1)
            except ValueError as e:
                if ('no tzinfo set' in e.args):
                    pass
            except Exception as e:
                print(e.args)
                time.sleep(60)
                # print(re.search(r'(.*)\n', e.args[0]).group(0))
                exit()
            os.remove(push_file)
        print('Dump %s pushed' % dir_file)

    def cutter_file(self, file, size):
        sep = config['DF']['sep']
        file = os.path.join(os.path.dirname(__file__), 'data', file)
        col = self.__read_df_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
        df = pd.read_csv(file, sep=sep, low_memory=False, header=0, error_bad_lines=False, warn_bad_lines=True)
        size = int(size)
        s = math.ceil(len(df) / size)
        if (size > 1):
            for i in range(1, s):
                df_cut, df = df[:size], df[size:]
                self.__write_df_csv(df_cut, os.path.abspath(file) + '_' + str(i * 100000) + '.csv', sep=sep, index=None)
                # df_cut.to_csv(os.path.abspath(file)+'_'+str(i)+'.csv', sep=sep,index=None)
                del (df_cut)
            self.__write_df_csv(df, file, sep=sep, index=None)

    def push_dump_file(self):
        dir = os.path.dirname(self.csvFile)
        dir_file = []
        for file in os.listdir(dir):
            file = re.search(r"(.*)dump(.*)", file)
            if (file):
                if '#' not in file.group(0) and 'push' not in file.group(0):
                    dir_file.append(file.group(0))
        sep = config['DF']['sep']

        for file in dir_file:
            file = os.path.join(dir, file)
            push_file = os.path.join(dir, file) + "_push.csv"

            while os.path.isfile(push_file) == False:
                try:
                    os.rename(file, push_file)
                    break
                except Exception:
                    pass
            col = self.__read_df_csv(push_file, sep=sep, nrows=0, low_memory=False)
            columns = ', '.join(col.columns)

            command = (
                "clickhouse-client --format_csv_delimiter=\"{}\"  --query=\"INSERT INTO {}.{} ({}) FORMAT CSVWithNames\" < {}").format(
                sep, self.clickhouse_database, self.clickhouse_table, columns, push_file)
            if 'l' in sys.argv:
                while True:
                    try:
                        result = str(os.system(command))
                        if result != '0':
                            print('I am sleep. Result:', result)
                            time.sleep(10)
                    except Exception as e:
                        print('Exception:', result)
                        print(e)
                        break
                    self.__delete_file(push_file)
                    break
            else:
                try:
                    result = str(os.system(command))
                    if result != '0':
                        print('Error. Result:', result)
                except Exception as e:
                    print('Exception:', result)
                    print(e)
                    exit()
                if result == '0':
                    self.__delete_file(push_file)
                    print('File %s pushed' % os.path.basename(str(file)))

    def push_dump_files(self):
        dir = os.path.dirname(self.csvFile)
        dir_file = []
        for file in os.listdir(dir):
            file = re.search(r"(.*)dump(.*)", file)
            if (file):
                if '#' not in file.group(0) \
                        and 'push' not in file.group(0) \
                        and 'collected' not in file.group(0) \
                        and 'dump.csv' != file.group(0):
                    dir_file.append(file.group(0))
        sep = config['DF']['sep']
        for file in dir_file:
            file = os.path.join(dir, file)
            push_file = os.path.join(dir, file) + "_push.csv"

            while os.path.isfile(push_file) == False:
                try:
                    os.rename(file, push_file)
                    break
                except Exception:
                    pass
            try:
                df = self.__read_df_csv(push_file, sep=sep, index_col=None, low_memory=False)
                # df = pd.read_csv(push_file, sep=sep, index_col=None, low_memory=False)
                if self.date_column not in df.columns:
                    col = self.__read_df_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
                    # col = pd.read_csv(self.csvFile, sep=sep, nrows=0, low_memory=False)
                    df.columns = col.columns
                df.dropna(subset=['Date', 'CounterId'], inplace=True)
                df[self.date_column] = df[self.date_column].str.replace('.', '-')
            except Exception as e:
                print(e.args)
                print('dump.csv for columns name does not exists')
                exit()

            '''
            if(len(df)<self.final_size):
                print(('File {} is small. I am exit').format(file))
                while os.path.isfile(file) == False:
                    try:
                        os.rename(push_file,file)
                        break
                    except Exception:
                        pass
                exit()
            '''
            columns = ', '.join(df.columns)
            del (df)
            command = (
                "clickhouse-client --format_csv_delimiter=\"{}\"  --query=\"INSERT INTO {}.{} ({}) FORMAT CSVWithNames\" < {}").format(
                sep, self.clickhouse_database, self.clickhouse_table, columns, push_file)
            if 'l' in sys.argv:
                while True:
                    try:
                        result = str(os.system(command))
                        if result != '0':
                            print('I am sleep. Result:', result)
                            time.sleep(10)
                    except Exception as e:
                        print('Exception:', result)
                        print(e)
                        break
                    self.__delete_file(push_file)
                    break
            else:
                try:
                    result = str(os.system(command))
                    if result != '0':
                        print('I am exit. Result:', result)
                        exit()
                except Exception as e:
                    print('Exception:', result)
                    print(e)
                    exit()
                self.__delete_file(push_file)
            time.sleep(1)
        print('File %s pushed' % os.path.basename(str(file)))
