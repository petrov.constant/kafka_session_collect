import os
import signal
import sys
import re
import subprocess
from datetime import datetime

from config import *

from flask import Flask, request, send_from_directory
from flask_cors import CORS, cross_origin
from kafka import KafkaProducer, errors

import ast

try:
    producer = KafkaProducer(bootstrap_servers=ast.literal_eval(config['KAFKA']['servers']))
    collector = "{0}/collector.py".format(config['DIR']['current'])
    pusher = "{0}/push_files.py".format(config['DIR']['current'])
    collector = subprocess.Popen([sys.executable,collector,' > /dev/null &'])
    pusher = subprocess.Popen([sys.executable,pusher,' > /dev/null &'])
    print('collector:',collector.pid, collector)
    print('pusher:',pusher.pid, pusher)
except Exception as e:
    os.kill(collector.pid, signal.SIGTERM)
    os.kill(pusher.pid, signal.SIGTERM)
    print(e)
    exit()
    '''
    kafka_start = "{0}/bin/kafka-server-start.sh -daemon {0}/config/server.properties".format(
        os.path.join(config['DIR']['current'], config['KAFKA']['dir'])
    )

    zookeeper_start = "{0}/bin/zookeeper-server-start.sh -daemon {0}/config/zookeeper.properties".format(
        os.path.join(config['DIR']['current'], config['KAFKA']['dir'])
    )
    try:
        k = subprocess.Popen(kafka_start.split(), shell=True)
        z = subprocess.Popen(zookeeper_start.split(), shell=True)
    except Exception as e2:
        print(e2)
        exit()
    '''

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

new = []
for domain in list(ast.literal_eval((config['APP']['good_domains']))):
    new.append('http://' + domain)
    new.append('.' + domain)
    new.append('https://' + domain)
    new.append('http://www.' + domain)
    new.append('https://www.' + domain)
    new.append('http://*.' + domain)
    new.append('https://*.' + domain)
good_domains = new
del (new, domain)


@app.route('/')
def hello_world():
    return 'Tell me a story, traveler...'


@app.route('/dumps')
def dumps():
    path = config['DIR']['data']
    list_of_files = []

    def convert_bytes(num):
        for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
            if num < 1024.0:
                return "%3.1f %s" % (num, x)
            num /= 1024.0

    def file_size(file_path):
        if os.path.isfile(file_path):
            file_info = os.stat(file_path)
            return convert_bytes(file_info.st_size)

    def count_lines(filename, chunk_size=1 << 13):
        with open(filename, 'r', encoding='utf-8') as file:
            return sum(chunk.count('\n')
                       for chunk in iter(lambda: file.read(chunk_size), ''))

    for filename in os.listdir(path):
        file = re.search(r"(.*).csv", filename)
        path_filename = os.path.join(path,filename)
        mod_timestamp = datetime.fromtimestamp(os.stat(path_filename).st_mtime).strftime('%H:%M:%S %Y.%m.%d')

        if (file):
            if '#' not in file.group(0):
                list_of_files.append(("<a href='dumps/{0}'>{0}</a> - {1} - {2} lines [{3}]<br/>").format(filename,
                                                                                                         file_size(
                                                                                                             path_filename),
                                                                                                         count_lines(
                                                                                                             path_filename),
                                                                                                         mod_timestamp))
    html = '<br /> '.join(list_of_files)
    return html


@app.route('/dumps/<filename>', methods=['GET'])
def download(filename):
    print(filename)
    return send_from_directory(directory='data', filename=filename)


@app.route('/google951fd5cde90534d8.html')
def google_staic():
    return 'google-site-verification: google951fd5cde90534d8.html'


@app.route('/robots.txt')
def robots_staic():
    with open('static/robots.txt', 'r') as f:
        robots = f.read()
    return robots, 200, {'Content-Type': 'text/css; charset=utf-8'}


@app.route('/collect', methods=['POST', 'GET'])
@cross_origin(origin=good_domains, supports_credentials=True)
def collect():
    if 'HTTP_X_FORWARDED_FOR' in request.environ:
        ip = request.environ['HTTP_X_FORWARDED_FOR']
    else:
        ip = request.environ['REMOTE_ADDR']

    if request.method == 'GET':
        items = dict(request.args.items())
    else:  # POST
        items = {}
        for key in request.values:
            items[key] = request.values[key]

    # stop keyvalue filter
    for key, value in items.items():
        for stop_pair in set(config['APP']['stop_keyval']):
            if key == stop_pair[0] and value == stop_pair[1]:
                return 'ok'

    # good keys filter
    items['Date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    items['ip'] = ip
    items = {k: v for k, v in items.items() if k in ast.literal_eval(config['CLICKHOUSE']['columns'])}
    producer.send(config['KAFKA']['topic'], str(items).encode())

    return 'ok'


if __name__ == '__main__':
    app.run(debug=True)
