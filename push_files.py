from time import sleep
from config import *
import ga2clickhouse

conn = ga2clickhouse.GA2clickhouse()

try:
    while True:
        conn.push_dump_file()
        sleep(int(config['APP']['push_sleep']))
except Exception as e:
    print(e)
    conn.push_dump_file()
